/**
 * Copyright 2012 Gabriel Pickl.
 * 
 * This file is part of reMap.
 * 
 * reMap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * reMap is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with reMap.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Gabriel Pickl
 * @version 0.2
 */

if(!THREE) { //This Project needs the three.js library
	throw 'Three.js library needed! ( https://github.com/mrdoob/three.js/ )';
}
if(!requestAnimFrame) {
	throw 'requestAnimFrame needed';
}

var CAMERA_ANGLE = 30;
var ZOOM_NEAR = 0.1;
var ZOOM_FAR = 10;
var ZOOM_TOLERANCE = 0.1;
var Y_MULTIPLIER = -1; //Because map-y extends downwards while scene-y extends upwards

goog.provide('ReMap.Map');

goog.require('ReMap.Tile');

/**
 * Map class. Represents an interactive map application
 * 
 * @param	opts	Object containing parameters
 * @param	opts.mask	The link mask. the strings {x},{y} will be replaced by the coordinates
 * @param	opts.size	Size of the map (in chunks)
 * @param	opts.offset	Initial offset of the camera
 * @param	opts.zoom_near	Closest zoom level
 * @param	opts.zoom_far	Farthest zoom level
 */
ReMap.Map = function(opts) {
	var _zoom_near, _zoom_far, _renderer, _ratio, _projector, _camera, _offset, _width, _height;
	this.mask = opts.mask;
	this.size = opts.size;
	_offset = opts.offset !== undefined ? opts.offset : new THREE.Vector3(0, 0, 0);
	_offset.y *= Y_MULTIPLIER;
	_zoom_near = opts.zoom_near !== undefined ? opts.zoom_near : ZOOM_NEAR;
	_zoom_far = opts.zoom_far !== undefined ? opts.zoom_far : ZOOM_FAR;
	_width = opts.width;
	_height = opts.height;
	
	_renderer = this.getRenderer();
	_ratio = _width/_height;
	_renderer.setSize(_width, _height);
	_camera =  new THREE.PerspectiveCamera(CAMERA_ANGLE, _ratio, _zoom_near, _zoom_far);
	_camera.position.addSelf(_offset);
	
	_camera.position.z = _camera.near + (_camera.far-_camera.near)/2;
	
	this.renderer = _renderer;
	this.domElement = _renderer.domElement;
	this.scene = new THREE.Scene();
	this.scene.add(_camera);
	this.camera = _camera;
	
	
	_projector = new THREE.Projector();
	//vector which defines the relationship between screen coordinates and map coordinates
	this.screenVector = _projector.unprojectVector(new THREE.Vector3(1, 1, 0.5), _camera);
	this.screenVector.divideScalar(this.screenVector.z);
	this.needsUpdate = true;
	
	this.tiles = new THREE.Object3D();
	this.scene.add(this.tiles);
};

ReMap.Map.prototype = {
	
	/**
	 * Sets the zoom level (distance of the camera from the map)
	 * 
	 * @param	distance	new distance
	 * @param	center	center of scaling
	 * 
	 * @return	old distance
	 */
	zoom: function(distance, center) {
		var _oldDist = this.camera.position.z;
		center = center !== undefined ? center.multiplySelf(new THREE.Vector3(1, Y_MULTIPLIER, 0)) : this.camera.position.clone();
		center.z = 0;
		center.subSelf(this.camera.position);
		
		//This fails somewhat when using the CanvasRenderer. Seems to be abug in THREE.js
		if(distance < this.camera.near) distance = this.camera.near;
		if(distance > this.camera.far) distance = this.camera.far;
		center.multiplyScalar((distance-_oldDist)/center.z);
		this.camera.position.addSelf(center);
		
		return _oldDist;
	},
	
	/**
	 * @return THREE.Vector3	Position of the camera in map coordinates
	 */
	getPosition: function() {
		return this.camera.position.clone();
	},
	
	/**
	 * Sets the camera to a new position
	 * 
	 * @param	pos	The new position of the camera
	 * @return	Old position
	 */
	setPosition: function(pos) {
		var _oldpos = this.camera.position;
		this.camera.position = pos;
		return _oldpos;
	},
	
	/**
	 * Updates which tiles are displayed based on the position and size of the viewport.
	 */
	update: function() {
		this.needsUpdate = false;
		
		var _pos = this.screenVector.clone().multiplyScalar(this.camera.position.z);	
		_pos.x = Math.round(_pos.x)-1;
		_pos.y = Math.round(_pos.y)-1;
		_pos.z = 0;
		
		var _origin = new THREE.Vector3(
			Math.round(this.camera.position.x),
			Math.round(this.camera.position.y),
			0
		);
		
		var _target = _pos.clone().multiplyScalar(-2);
		
		_origin.addSelf(_pos);
		_pos = _origin.clone();
		_target.addSelf(_origin);
		_target.y += 1;
		
		var _yAxisMultiplier = new THREE.Vector3(1, Y_MULTIPLIER, 1);
		
		var _newTiles = new THREE.Object3D();
		while(_pos.y < _target.y) {
			if(
				_pos.x >= 0 &&
				_pos.y <= 0 &&
				_pos.x <= this.size.x &&
				_pos.y >= Y_MULTIPLIER * this.size.y
			) {
				var _tileCounter = this.tiles.children.length;
				var _tile = undefined;
				
				//Iterate over our current tiles to see if the needed one already exists
				while(_tileCounter--) {
					_tile = this.tiles.children[_tileCounter];
					if(
						_tile.position.x == _pos.x &&
						_tile.position.y == _pos.y
					) {
						break;
					} else {
						_tile = undefined;
					}
				}
				//If the needed tile wasn't in our collection...
				if(_tile === undefined) {
					_tile = new ReMap.Tile(_pos.clone(), this.mask);
				}
				
				_newTiles.add(_tile);
			}
			
			if(_pos.x == _target.x) {
				_pos.x = _origin.x;
				_pos.y -= Y_MULTIPLIER;
			} else {
				_pos.x++;
			}
		}
		
		this.scene.remove(this.tiles);
		this.scene.add(_newTiles);
		this.tiles = _newTiles;
	},
	
	/**
	 * Renders the scene to canvas
	 */
	render: function() {
		if(this.needsUpdate) this.update();
		var target = this.camera.position.clone();
		target.z = 0;
		
		this.camera.lookAt(target);
		this.renderer.render(this.scene, this.camera);
		requestAnimFrame(this.render.bind(this));
	},
	
	getRenderer: function() {
		return new THREE.WebGLRenderer();
	}
};