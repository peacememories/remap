/**
 * Copyright 2012 Gabriel Pickl.
 * 
 * This file is part of reMap.
 * 
 * reMap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * reMap is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with reMap.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Gabriel Pickl
 * @version 0.2
 */
if(!DRAG) { //This Project needs the three.js library
	throw 'Drag library needed!';
}
if(!SCROLL) {
	throw 'Scroll library needed!';
}

goog.require('ReMap.Map');
goog.provide('ReMap.Interactivity');

ReMap.Interactivity.SCROLL_FACTOR = 1.2;

/**
 * Makes the Map interactive
 */
ReMap.Map.prototype.makeInteractive = function() {
	this.handleEvent = ReMap.Interactivity.eventHandler.bind(this);
	
	var _el = this.domElement;
	_el.addEventListener('scrollWheel', this);
	_el.addEventListener('dragMove', this);
	_el.addEventListener('dragStop', this);
}

/**
 * Removes interactivity from the map
 */
ReMap.Map.prototype.makeNonInteractive = function() {
	var _el = this.domElement;
	_el.removeEventListener('scrollWheel', this);
	_el.removeEventListener('dragMove', this);
	_el.removeEventListener('dragStop', this);
}

ReMap.Interactivity.eventHandler = function(ev) {
	/**
	 * Converts relative screenspace coordinates to relative mapspace coordinates
	 * 
	 * @param	screenCoords	relative coordinates
	 * @param	map
	 */
	function mapCoords(screenCoords, map) {
		screenCoords.multiplySelf(map.screenVector);
		screenCoords.multiplyScalar(map.camera.position.z);
		return screenCoords;
	}
	var _el = this.domElement;
	switch(ev.type) {
		case 'dragMove':
			var _screenVector = new THREE.Vector3(
				2*ev.detail.dX/_el.clientWidth,
				Y_MULTIPLIER*2*ev.detail.dY/_el.clientHeight,
				0
			);
			this.camera.position.addSelf(
				mapCoords(_screenVector, this)
			);
			break;
		case 'scrollWheel':
			var _direction = ReMap.Interactivity.SCROLL_FACTOR;
			if(ev.detail) _direction = 1/_direction;
			var _screenVector = new THREE.Vector3(
				2*ev.clientX/_el.clientWidth-1,
				Y_MULTIPLIER*2*ev.clientY/_el.clientHeight-Y_MULTIPLIER,
				0
			);
			_screenVector = mapCoords(_screenVector, this);
			var _offsetVector = this.camera.position.clone().subSelf(_screenVector);
			_offsetVector.y *= Y_MULTIPLIER;
			this.zoom(this.camera.position.z*_direction, _offsetVector);
		case 'dragStop':
			this.needsUpdate = true;
	}
}