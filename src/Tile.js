goog.provide('ReMap.Tile');

ReMap.Tile = function(position, mask) {
	if(typeof mask === 'function') {
		mask = mask.call(this,position);
	} else {
		mask = mask.replace('{x}', position.x);
		mask = mask.replace('{y}', -position.y);
	}
	THREE.Mesh.call(this,
		new THREE.PlaneGeometry(1, 1),
		new THREE.MeshBasicMaterial({
			map: THREE.ImageUtils.loadTexture(mask),
			overdraw: true
		})
	);
	this.position.addSelf(position);
};

ReMap.Tile.prototype = new THREE.Mesh();
ReMap.Tile.prototype.constructor = ReMap.Tile;
ReMap.Tile.prototype.supr = THREE.Particle.prototype;