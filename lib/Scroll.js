/**
 * Copyright 2011, 2012 Gabriel Pickl.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Gabriel Pickl
 */

window.SCROLL = 'v0.1';

function handleScroll(event) {
	var delta;
	if(event.wheelDelta !== undefined) {
		delta = event.wheelDelta > 0;
	} else {
		delta = event.detail < 0;
	}
	var ev = document.createEvent('MouseEvent');
	ev.initMouseEvent(
		'scrollWheel',
		true, true,
		event.view,
		delta,
		event.screenX,
		event.screenY,
		event.clientX,
		event.clientY,
		event.ctrlKey,
		event.altKey,
		event.shiftKey,
		event.metaKey,
		event.button,
		null
	);
	event.target.dispatchEvent(ev);
	if(delta) {
		var ev = document.createEvent('MouseEvent');
		ev.initMouseEvent(
			'scrollWheelUp',
			true, true,
			event.view,
			delta,
			event.screenX,
			event.screenY,
			event.clientX,
			event.clientY,
			event.ctrlKey,
			event.altKey,
			event.shiftKey,
			event.metaKey,
			event.button,
			null
		);
		event.target.dispatchEvent(ev);
	} else {
		var ev = document.createEvent('MouseEvent');
		ev.initMouseEvent(
			'scrollWheelDown',
			true, true,
			event.view,
			delta,
			event.screenX,
			event.screenY,
			event.clientX,
			event.clientY,
			event.ctrlKey,
			event.altKey,
			event.shiftKey,
			event.metaKey,
			event.button,
			null
		);
		event.target.dispatchEvent(ev);
	}
}

window.addEventListener('DOMMouseScroll', handleScroll);
window.addEventListener('mousewheel', handleScroll);