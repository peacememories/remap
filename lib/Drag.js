/**
 * Copyright 2011, 2012 Gabriel Pickl.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Gabriel Pickl
 */

window.DRAG = 'v0.1';

function setupDrag() {
	document.addEventListener('mousedown', function(event) {
		var target = event.target;
		var ev = document.createEvent('CustomEvent');
		var startX = event.clientX;
		var startY = event.clientY;
		var x = startX; var y = startY;
		var button = event.button;
		ev.initCustomEvent(
			'dragStart',
			true,
			true,
			null
		);
		target.dispatchEvent(ev);
		var dragListener = function(event) {
			var ev = document.createEvent('CustomEvent');
			ev.initCustomEvent(
				'dragMove',
				true,
				true,
				{
					x: event.clientX - startX,
					y: event.clientY - startY,
					dX: event.clientX - x,
					dY: event.clientY - y,
					button: button
				}
			);
			x = event.clientX;
			y = event.clientY;
			target.dispatchEvent(ev);
		};
		var stopListener = function(event) {
			var ev = document.createEvent('CustomEvent');
			ev.initCustomEvent(
				'dragStop',
				true,
				true,
				{
					x: event.clientX - startX,
					y: event.clientY - startY,
					button:button
				}
			);
			
			target.dispatchEvent(ev);
			window.removeEventListener('mousemove', dragListener);
			window.removeEventListener('mouseup', stopListener);
		};
		window.addEventListener('mousemove', dragListener);
		window.addEventListener('mouseup', stopListener);
	});
}
setupDrag();
