#!/bin/bash
SRC_PATH="src"
COMPILER="java -jar utils/compiler/compiler.jar"
BIN_LICENSE="LICENSE"
FILES=(
	"ReMap.js"
	"Map.js"
	"Interactivity.js"
	"Tile.js"
)

OPTS="
--manage_closure_dependencies
--closure_entry_point ReMap
"

for i in ${FILES[@]}
do
	_files=$_files" --js "$SRC_PATH/$i
done


_command="$COMPILER $_files $OPTS $@"

OUTPUT=$($_command)
cat $BIN_LICENSE | sed "s/^/\/\//"
echo
echo "$OUTPUT"