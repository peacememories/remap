EXAMPLE_DIR=examples
EXAMPLES=(
	"simple_map"
)
BUILD_CORE=$(dirname $0)/build_core.sh

_message="$BUILD_CORE $@"

for i in ${EXAMPLES[@]}
do
	echo "$($_message)" > $EXAMPLE_DIR/$i/remap.js
done
